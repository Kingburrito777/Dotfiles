#!/usr/bin/env bash

# Kill already running
killall -q polybar

# Launch bars foreach monitor
echo "---" | tee -a /tmp/polybarTop.log /tmp/polybarBot.log

MONITOR="eDP1" polybar top >>/tmp/polybarTop.log 2>&1 & disown
MONITOR="eDP1" polybar bot >>/tmp/polybarBot.log 2>&1 & disown

MONITOR="HDMI1" polybar top >>/tmp/polybarBot.log 2>&1 & disown
MONITOR="HDMI1" polybar bot >>/tmp/polybarBot.log 2>&1 & disown

echo "Launched polybar"
